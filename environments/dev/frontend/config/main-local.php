<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
    ////////////kartik extention
    $config['modules']['gridview'] = [
        'class' => '\kartik\grid\Module',
    ];

    $config['modules']['datecontrol'] = [
        'class' => '\kartik\datecontrol\Module',
    ];
    
    $config['modules']['dynagrid'] = [
        'class' => '\kartik\dynagrid\Module',
    ];
    $config['components']['assetManager']['forceCopy'] = true;
    ///////kartik gii
//    $config['modules']['gii']['class'] = 'yii\gii\Module';
//    
//    $config['modules']['gii']['generators'] = [
//        'kartikgii-crud' => ['class' => 'warrence\kartikgii\crud\Generator'],
//    ];
}

return $config;
