<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\MesyJawatanKuasa */

$this->title = 'Create Mesy Jawatan Kuasa';
$this->params['breadcrumbs'][] = ['label' => 'Mesy Jawatan Kuasa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mesy-jawatan-kuasa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
