<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\MesyJawatanKuasa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mesy-jawatan-kuasa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tajuk_mesy')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'tarikh_mesy')->widget(
            DatePicker::className(), [
        'value' => date('Y-m-d H:i:s'),
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>

    <?= $form->field($model, 'ahli1')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'ahli2')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'ahli3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'siri')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
