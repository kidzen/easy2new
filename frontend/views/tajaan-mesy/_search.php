<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TajaanMesySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tajaan-mesy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_indent') ?>

    <?= $form->field($model, 'id_mesy') ?>

    <?= $form->field($model, 'id_syarikat') ?>

    <?= $form->field($model, 'id_agse_agsv') ?>

    <?php // echo $form->field($model, 'no_kontrak') ?>

    <?php // echo $form->field($model, 'jenis_tajaan') ?>

    <?php // echo $form->field($model, 'harga') ?>

    <?php // echo $form->field($model, 'edd_serah') ?>

    <?php // echo $form->field($model, 'edd_terima') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
