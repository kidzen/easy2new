<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\TajaanMesy */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tajaan Mesyuarat', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tajaan-mesy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_indent',
//            'id_mesy',
            [
                'label' => 'Siri Mesyuarat',
                'value' => $model->idMesy->siri
            ],
            [
                'label' => 'Tajuk Mesyuarat',
                'value' => $model->idMesy->tajuk_mesy
            ],
//            'id_syarikat',
            [
                'label' => 'Siri Mesyuarat',
                'value' => $model->idSyarikat->nama_syarikat
            ],
//            'id_agse_agsv',
            [
                'label' => 'No Daftar Kenderaan',
                'value' => $model->idAgseAgsv->no_daftar
            ],
            [
                'label' => 'Jenis Kenderaan',
                'value' => $model->idAgseAgsv->jenis_agsv_agse
            ],
            [
                'label' => 'Gambar Kenderaan',
                'format'=>['image',['width'=>'auto','height'=>'200']],
                'value' => $model->idAgseAgsv->url_gambar
            ],
            'no_kontrak',
            [
                'label' => 'Indent Kontrak',
                'value' => $model->noKontrak->no_indent
            ],
            [
                'label' => 'Had Bumbung Kontrak Penuh',
                'value' => $model->noKontrak->had_bumbung
            ],
            'jenis_tajaan',
            'harga',
            'edd_serah',
            'edd_terima',
            'created_date',
            'updated_date',
        ],
    ])
    ?>

</div>
