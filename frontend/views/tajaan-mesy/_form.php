<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use dosamigos\datepicker\DatePicker;
//use frontend\models\MaklumatIndent;
use frontend\models\MesyJawatanKuasa;
use frontend\models\MaklumatAgsvAgse;
use frontend\models\MaklumatSyarikat;
use frontend\models\ButirKontrak;

/* @var $this yii\web\View */
/* @var $model frontend\models\TajaanMesy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tajaan-mesy-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model, 'no_indent')->textInput(['maxlength' => true,'readonly'=>true,'value'=>'PUGK-TAJAAN-'.date('ymd').'/'.$bilTajaan]) ?>

    <?=
    $form->field($model, 'id_mesy')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(MesyJawatanKuasa::find()->all(), 'id', 'tajuk_mesy'),
        'options' => ['placeholder' => 'Select a indent...'],
    ]);
    ?>
    
    <?=
    $form->field($model, 'id_agse_agsv')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(MaklumatAgsvAgse::find()->all(), 'id', 'no_daftar'),
        'options' => ['placeholder' => 'Select a indent...'],
    ]);
    ?>

    <?=
    $form->field($model, 'no_kontrak')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(ButirKontrak::find()->all(), 'no_kontrak', 'no_kontrak'),
        'options' => ['placeholder' => 'Select a indent...'],
    ]);
    ?>

    <?php
//    echo $form->field($model, 'jenis_tajaan')->widget(Select2::classname(), [
//        'data' => ['KONTRAK' => 'KONTRAK', 'TAJAAN' => 'TAJAAN'],
//        'options' => ['placeholder' => 'Select a indent...'],
//    ]);
    ?>

    <?= $form->field($model, 'harga')->textInput() ?>

    <?=
    $form->field($model, 'edd_serah')->widget(
            DatePicker::className(), [
        'value' => date('Y-m-d H:i:s'),
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'edd_terima')->widget(
            DatePicker::className(), [
        'value' => date('Y-m-d H:i:s'),
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
