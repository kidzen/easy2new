<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TajaanMesy */

$this->title = 'Create Tajaan Mesyuarat';
$this->params['breadcrumbs'][] = ['label' => 'Tajaan Mesyuarat', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tajaan-mesy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'bilTajaan' => $bilTajaan,
    ]) ?>

</div>
