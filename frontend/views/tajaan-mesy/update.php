<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\TajaanMesy */

$this->title = 'Update Tajaan Mesyuarat: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tajaan Mesyuarat', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tajaan-mesy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form2', [
        'model' => $model,
    ]) ?>

</div>
