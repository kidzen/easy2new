<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\MaklumatSyarikat */

$this->title = 'Update Maklumat Syarikat: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Maklumat Syarikat', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="maklumat-syarikat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
