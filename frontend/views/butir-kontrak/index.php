<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Butir Kontrak';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butir-kontrak-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Butir Kontrak', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'no_indent',
            'no_kontrak',
//            'id_syarikat',
            [
                'label' => 'Nama Syarikat',
                'attribute' => 'syarikat_nama_syarikat',
                'value' => 'idSyarikat.nama_syarikat',
            ],
            'had_bumbung',
            'tarikh_mula',
            'tarikh_tamat_kontrak',
            'revenue_kontrak',
            'created_date',
            'updated_date',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
