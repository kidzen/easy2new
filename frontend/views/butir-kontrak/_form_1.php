<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use kartik\widgets\ActiveForm;
//use kartik\builder\Form;
//use yii\jui\DatePicker;
//use yii\bootstrap;
//use kartik\datetime\DateTimePicker;
//use kartik\date\DatePicker;
use dosamigos\datepicker\DatePicker;

//use kartik\date;
/* @var $this yii\web\View */
/* @var $model frontend\models\ButirKontrak */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="butir-kontrak-form">


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_indent')->textInput() ?>

    <?= $form->field($model, 'no_kontrak')->textInput(['maxlength' => true])->hint('Hint Text') ?>

    <?= $form->field($model, 'had_bumbung')->textInput(['maxlength' => true]) ?>
    <!--form-control-->

    <?=
    $form->field($model, 'tarikh_mula')->widget(
            DatePicker::className(), [
        'value' => date('Y-m-d H:i:s'),
        // modify template for custom rendering
//        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'tarikh_jv')->widget(
            DatePicker::className(), [
        // inline too, not bad
        'inline' => false,
        // modify template for custom rendering
//        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'tarikh_sst')->widget(
            DatePicker::className(), [
        // inline too, not bad
        'inline' => false,
        // modify template for custom rendering
//        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>



    <?=
    $form->field($model, 'tarikh_tt_kontrak')->widget(
            DatePicker::className(), [
        // inline too, not bad
        'inline' => false,
        // modify template for custom rendering
//        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>

    <?= $form->field($model, 'revenue_kontrak')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<!--<div class="hero-unit">
    <input  type="text" placeholder="click to show datepicker"  id="example1">
</div>-->
<!-- <script src="js/jquery-1.9.1.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>-->
<?php
//$this->registerJsFile('pugin/datepicker/bootstrap-datepicker.js');
//echo $this->registerJsFile("js\customjs.js");
//$script = <<< JS
////            
//            $(document).ready(function () { $("#example1").datepicker(); });  
////            $("document").ready(function(){ alert("hi"); });
//JS;
//$this->registerJs($script);
?>

<?php
//$this->registerJs(
//    '$("document").ready(function(){ alert("hi"); });'
//    '$(document).ready(function () {
//        $("#example1").datepicker({
//        format: "dd/mm/yyyy"
//        }); '
//);
?>
<?php

//$js = <<< JS
////        if($('#revenue_kontrak').attr('checked')) {
////                alert();
////   //            $(".field-butirkontrak-revenue_kontrak").show();
////        } else {
////                alert();
//////            $(".field-butirkontrak-revenue_kontrak").hide();
////        };
//        
////   $('#revenue_kontrak').change(function(){ 
////        if($('#revenue_kontrak').attr('checked')) {
////                alert('yes');
//////   //            $(".field-butirkontrak-revenue_kontrak").show();
////        } else {
////                alert('no');
////////            $(".field-butirkontrak-revenue_kontrak").hide();
////        };
////   var editRevenue = $(this).val();
//////        var editRevenue = $('#revenue_kontrak').attr('checked');
//////       alert(editRevenue); 
//////           $(".field-butirkontrak-revenue_kontrak").hide();
//////       $.get(); 
////           });
//        
//        $('#revenue_kontrak').click(function() {
//            $(".field-butirkontrak-revenue_kontrak").toggle(this.checked);
//        });
//JS;
//$this->registerJs($js);