<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use frontend\models\MaklumatAgsvAgse;
use kartik\select2\Select2;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\SejarahPembaikan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sejarah-pembaikan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_indent')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'id_agsv_agse')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(MaklumatAgsvAgse::find()->all(), 'id', 'no_daftar'),
        'options' => ['placeholder' => 'Select a indent...'],
    ]);
    ?>

    <?= $form->field($model, 'jenis_pembaikan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lst_alat_ganti')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qty_alat_ganti')->textInput() ?>

    <?= $form->field($model, 'harga_alat_ganti')->textInput() ?>

    <?=
    $form->field($model, 'tarikh_terima')->widget(
            DatePicker::className(), [
        'value' => date('Y-m-d H:i:s'),
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>
    
    <?=
    $form->field($model, 'tarikh_siap')->widget(
            DatePicker::className(), [
        'value' => date('Y-m-d H:i:s'),
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
