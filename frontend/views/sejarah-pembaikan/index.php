<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\SejarahPembaikanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sejarah Pembaikan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sejarah-pembaikan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sejarah Pembaikan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_indent',
            'id_agsv_agse',
            'jenis_pembaikan',
            'lst_alat_ganti',
            // 'qty_alat_ganti',
            // 'harga_alat_ganti',
            // 'tarikh_terima',
            // 'tarikh_siap',
            // 'created_date',
            // 'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
