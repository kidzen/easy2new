<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\SejarahPembaikan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sejarah Pembaikan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sejarah-pembaikan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_indent',
            'id_agsv_agse',
            'jenis_pembaikan',
            'lst_alat_ganti',
            'qty_alat_ganti',
            'harga_alat_ganti',
            'tarikh_terima',
            'tarikh_siap',
            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>
