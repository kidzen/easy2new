<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\SejarahPembaikanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sejarah-pembaikan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_indent') ?>

    <?= $form->field($model, 'id_agsv_agse') ?>

    <?= $form->field($model, 'jenis_pembaikan') ?>

    <?= $form->field($model, 'lst_alat_ganti') ?>

    <?php // echo $form->field($model, 'qty_alat_ganti') ?>

    <?php // echo $form->field($model, 'harga_alat_ganti') ?>

    <?php // echo $form->field($model, 'tarikh_terima') ?>

    <?php // echo $form->field($model, 'tarikh_siap') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
