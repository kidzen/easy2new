<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MaklumatIndentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Maklumat Indent';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maklumat-indent-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Maklumat Indent', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php    Pjax::begin(['id'=>'pjax-job-gridview']) ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
//        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            'no_indent',
//            'no_kontrak',
            [
                'label'=>'Nama Syarikat',
//                'attribute'=>'syarikat_nama_syarikat',
                'value'=>'syarikat.nama_syarikat'
            ],
            'jenis_indent',
            'jumlah_indent',
            'baki_had_bumbumg',
            'created_date',
            'updated_date',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php    Pjax::end() ?>

</div>
