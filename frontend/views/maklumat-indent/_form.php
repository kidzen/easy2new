<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MaklumatIndent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maklumat-indent-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_indent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_kontrak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis_indent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jumlah_indent')->textInput() ?>

    <?= $form->field($model, 'baki_had_bumbumg')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
