<?php

use yii\helpers\Html;
use yii\db\BaseActiveRecord;
use frontend\models\TajaanMesy;

/* @var $this \yii\web\View */
/* @var $content string */
$siries = frontend\models\MesyJawatanKuasa::find()->asArray()->all();
//var_dump($tajaan["id"]);
//var_dump($tajaan);
//die;
?>

<header class="main-header">

    <?php echo Html::a('<span class="logo-mini">APP</span><span class="logo-lg">TUDM</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">
        <span style="margin-left: 230px; line-height: 200%; color: #ffffff; font-weight: 500; font-size: 25px;">
            Sistem Pengurusan Kontraktorisasi AGSE / AGSV
        </span>
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu" >

            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                         <!--User image--> 
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?= Yii::$app->user->name ?>
                                <small><?= Yii::$app->user->role ?></small>
                            </p>
                        </li>
                         <!--Menu Body--> 
                        
                         <!--Menu Footer-->
                        <li class="user-footer">
                            <?php if (Yii::$app->user->id){?>
                            <div class="pull-left">
                                <!--<a href="#" class="btn btn-default btn-flat">Profile</a>-->
                                <?= Html::a(
                                    'Profile',
                                    ['/user/view','id'=>Yii::$app->user->id],
                                    ['class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                            <?php } else { ?>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign in',
                                    ['/site/login'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                            <?php } ?>
                        </li>
                        <!--End Menu Footer-->
                    </ul>
                </li>
            </ul>
        </div>


        <!--        <div class="navbar-custom-menu" >
        
                    <ul class="nav navbar-nav">
        
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> JTP
                                <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <ul class="menu">
        <?php foreach ($siries as $siri) { ?>
                                                    <li>
                                                        <a href="http://localhost/easy2/frontend/web/index.php?r=tajaan-mesy%2Findex">
                                                            <i class="fa fa-users text-aqua"></i><?php echo $siri["siri"]; ?>
                                                        </a>
                                                    </li>
        <?php } ?>
                                    </ul>
                                </li>
                                
                                <li>
                                    <ul class="menu">
                                            <li>
                                                <a href="#" class="dropdown-toggle" data-toggle="menu">
                                                    hai
                                                </a>
                                            </li>
                                    </ul>
                                </li>
                                
        
                                <li class="footer"><a href="#">View all</a></li>
                            </ul>
                        </li>
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> JTP
                                <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <ul class="menu">
        <?php foreach ($siries as $siri) { ?>
                                                    <li>
                                                        <a href="http://localhost/easy2/frontend/web/index.php?r=tajaan-mesy%2Findex">
                                                            <i class="fa fa-users text-aqua"></i><?php echo $siri["siri"]; ?>
                                                        </a>
                                                    </li>
        <?php } ?>
                                    </ul>
                                </li>
                                <li>
                                    <ul class="menu">
                                        <li>
                                            <a href="http://localhost/easy2/frontend/web/index.php?r=tajaan-mesy%2Findex">
                                                <i class="fa fa-users text-aqua"></i>hai
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">View all</a></li>
                            </ul>
                        </li>
        
                    </ul>
                </div>-->
    </nav>
    

</header>
