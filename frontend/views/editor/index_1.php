<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
//foreach ($dataProvider as $key){
//    echo $key.toS."<br>";
//}
//echo '<pre>';
//var_dump($dataProvider2);die;

//echo '</pre>';
//asort($dataProvider);
//var_dump($dataProvider);
//print_r($dataProvider);
?>
<div class="maklumat-syarikat-index">

    <h1>Summary</h1>

<!--    <p>
        <?= Html::a('Create Maklumat Syarikat', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
    <hr>
    <p>
        <!--Choose which data should be shown:-->
    </p>
<!--    <input type='checkbox' class="checkbox-inline" style="margin-left: 10px">&nbsp;&nbsp;Option 1
    <input type='checkbox' class="checkbox-inline" style="margin-left: 10px">&nbsp;&nbsp;Option 2-->

    <hr>
    <?php // echo 

$columns = 
[
    [
        'class'=>'kartik\grid\SerialColumn', 
        'pageSummary'=>'Page Total',
        'order'=>DynaGrid::ORDER_FIX_LEFT
    ],

//            //Tajaan Mesy

//    'no_indent',
//     'edd_serah',
//     'edd_terima',
    [
        'label'=>'Id Tajaan',
        'attribute'=>'id',
        'vAlign'=>'middle',
    ],
    [
        'label'=>'EDD Serah',
        'attribute'=>'edd_serah',
        'vAlign'=>'middle',
    ],
    [
        'label'=>'EDD Terima',
        'attribute'=>'edd_terima',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'jenis_tajaan',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'harga',
        'vAlign'=>'middle',
        'pageSummary'=>true,
    ],
    [
        'label'=>'Tajaan created_date',
        'attribute'=>'created_date',
        'filterType'=>GridView::FILTER_DATE,
        'format'=>'raw',
        'width'=>'300px',
        'filterWidgetOptions'=>[
            'pluginOptions'=>['format'=>'yyyy-mm-dd']
        ],
        'visible'=>false,
    ],
    [
        'label'=>'Tajaan updated_date',
        'attribute'=>'updated_date',
        'filterType'=>GridView::FILTER_DATE,
        'format'=>'raw',
        'width'=>'300px',
        'filterWidgetOptions'=>[
            'pluginOptions'=>['format'=>'yyyy-mm-dd']
        ],
        'visible'=>false,
    ],
    
    //Maklumat Syarikat
    [
        'label'=>'Nama Syarikat',
        'attribute'=>'syarikat_nama_syarikat',
        'value'=>'idSyarikat.nama_syarikat',
        'vAlign'=>'middle',
    ],
    [
        'label'=>'Kod Bidang Syarikat',
        'attribute'=>'syarikat_kod_bidang',
        'value'=>'idSyarikat.kod_bidang',
        'vAlign'=>'middle',
    ],
    
    //Butir Kontrak
//    [
//        'attribute'=>'kontrak_id_indent',
//        'value'=>'noKontrak.id_indent',
//        'vAlign'=>'middle',
//    ],
    [
        'label'=>'Kontrak:No kontrak',
        'attribute'=>'kontrak_no_kontrak',
        'value'=>'noKontrak.no_kontrak',
//        'value'=>'noKontrak.no_kontrak',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'kontrak_had_bumbung',
        'value'=>'noKontrak.had_bumbung',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'kontrak_tarikh_mula',
        'vAlign'=>'middle',
        'filterType'=>GridView::FILTER_DATE,
        'format'=>'raw',
        'width'=>'170px',
        'filterWidgetOptions'=>[
            'pluginOptions'=>['format'=>'yyyy-mm-dd']
        ],
        'visible'=>false,
    ],
    [
        'attribute'=>'kontrak_tarikh_tamat',
        'vAlign'=>'middle',
        'filterType'=>GridView::FILTER_DATE,
        'format'=>'raw',
        'width'=>'170px',
        'filterWidgetOptions'=>[
            'pluginOptions'=>['format'=>'yyyy-mm-dd']
        ],
        'visible'=>false,
    ],
    [
        'attribute'=>'kontrak_tarikh_jv',
        'value'=>'noKontrak.tarikh_jv',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'kontrak_tarikh_sst',
        'value'=>'noKontrak.tarikh_sst',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'kontrak_tarikh_tamat_kontrak',
        'value'=>'noKontrak.tarikh_tamat_kontrak',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'kontrak_revenue_kontrak',
        'value'=>'noKontrak.revenue_kontrak',
        'vAlign'=>'middle',
    ],
    
    //Mesy
    [
        'attribute'=>'mesy_tajuk_mesy',
        'value'=>'idMesy.tajuk_mesy',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'mesy_tarikh_mesy',
        'value'=>'idMesy.tarikh_mesy',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'mesy_id_lst_ahli',
        'value'=>'idMesy.id_lst_ahli',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'mesy_siri',
        'value'=>'idMesy.siri',
        'vAlign'=>'middle',
    ],
    
//            //Maklumat Agsv Agse
//            'id',
//            'no_daftar',
//            'url_gambar:url',
//            'tarikh_masuk_khidmat',
//            'tarikh_serah_terima',
//            'created_date',
//            'updated_date',
//             
//             
//             
    //Kenderaan
    [
        'attribute'=>'kenderaan_no_daftar',
        'value'=>'idAgseAgsv.no_daftar',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'kenderaan_url_gambar',
        'value'=>'idAgseAgsv.url_gambar',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'kenderaan_tarikh_masuk_khidmat',
        'value'=>'idAgseAgsv.tarikh_masuk_khidmat',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'kenderaan_tarikh_serah_terima',
        'value'=>'idAgseAgsv.tarikh_serah_terima',
        'vAlign'=>'middle',
    ],
    
    //sej
//    [
//        'label'=>'jenis baiki',
//        'attribute' =>'sej_jenis_baiki',
//        'value' =>'idAgseAgsv.jenis_pembaikian',
//    ],
//    Indent
    [
        'label'=>'Debug',
//        'attribute' =>'noIndent.jumlah_indent',
        'attribute' =>'noIndent.jumlah_indent',
//        'attribute' =>'indent_jenis_indent',
        'value' =>'noIndent.jenis_indent',
    ],
//    [
//        'label'=>'Jenis Indent',
//        'attribute' =>'indent_jenis_indent',
//        'value' =>'noIndent.jenis_indent',
//    ],
    //Action
//    [
//        'class'=>'kartik\grid\ActionColumn',
//        'dropdown'=>false,
//        'urlCreator'=>function($action, $model, $key, $index) { return '#'; },
//        'viewOptions'=>['title'=>'view', 'data-toggle'=>'tooltip'],
//        'updateOptions'=>['title'=>'update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['title'=>'delete', 'data-toggle'=>'tooltip'], 
//        'order'=>DynaGrid::ORDER_FIX_RIGHT
//    ],
                
//    ['class'=>'kartik\grid\CheckboxColumn', 'order'=>DynaGrid::ORDER_FIX_RIGHT],
];
$dynagrid = DynaGrid::begin([
    'columns'=>$columns,
    'theme'=>'panel-info',
    'showPersonalize'=>true,
    'storage'=>'cookie',
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'showPageSummary'=>true,
        'floatHeader'=>true,
        'pjax'=>true,
        'panel'=>[
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  Library</h3>',
            'before' =>  '<div style="padding-top: 7px;"><em>* The table header sticks to the top in this demo as you scroll</em></div>',
            'after' => false
        ],        
        'toolbar' =>  [
            ['content'=>
                Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>'Add Book', 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['dynagrid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
            ],
            ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
            '{export}',
        ]
    ],
    'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
]);
if (substr($dynagrid->theme, 0, 6) == 'simple') {
    $dynagrid->gridOptions['panel'] = false;
}  
DynaGrid::end();





//    echo
//    GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//            
//            //Tajaan Mesy
//            'id',
//            'id_mesy',
//            'id_syarikat',
//            'id_agse_agsv',
//            'jenis_tajaan',
//            'qty',
//            'harga_per_unit',
//            'harga',
//            'created_date',
//            'updated_date',
//            
//            
//            //Maklumat Syarikat
//            'id',
//            [
//                'label'=>'Nama Syarikat',
//                'attribute'=>'nama_syarikat',
//                'headerOptions' => ['class' => 'nama_syarikat'],
//                'contentOptions' => ['class' => 'nama_syarikat'],
//            ],
//            'kod_bidang',
//            'created_date',
//            'updated_date',
//            
//            
//            //Maklumat Agsv Agse
//            'id',
//            'no_daftar',
//            'url_gambar:url',
//            'tarikh_masuk_khidmat',
//            'tarikh_serah_terima',
//            'created_date',
//            'updated_date',
//             
//             
//             
//             //Butir Kontrak
//            'id',
//            'id_indent',
//            'no_kontrak',
//            'had_bumbung',
//            'tarikh_mula',
//            'tarikh_jv',
//            'tarikh_sst',
//            'tarikh_tamat_kontrak',
//            'revenue_kontrak',
//            'created_date',
//            'updated_date',
//            
//            
//            //Mesy Jawatan Kuasa
//            'id',
//            'tajuk_mesy',
//            'tarikh_mesy',
//            'id_lst_ahli',
//            'siri',
//            'created_date',
//            'updated_date',
//             
//             
//             
//             
//             
//             
//             
//             
//             
//             
//             
//             
//             
////            [
////                'label'=>'Tajuk Mesy',
////                'attribute'=>'idMesy.tajuk_mesy',
//////                'value'=>'id_mesy.tajuk_mesy',
////                'headerOptions' => ['class' => 'nama_syarikat'],
////                'contentOptions' => ['class' => 'nama_syarikat'],
////            ],
////            [
////                'label'=>'Tajuk Mesy',
////                'attribute'=>'id_mesy',
////                'value'=>'idMesy.tajuk_mesy',
////                'headerOptions' => ['class' => 'nama_syarikat'],
////                'contentOptions' => ['class' => 'nama_syarikat'],
////            ],
//            [
//                'label'=>'Siri Mesy',
//                'attribute'=>'idMesy_siri',
//                'value'=>'idMesy.siri',
//                'headerOptions' => ['class' => 'nama_syarikat'],
//                'contentOptions' => ['class' => 'nama_syarikat'],
//            ],
//            [
//                'label'=>'Tajuk Mesy',
//                'attribute'=>'idMesy_tajuk',
//                'value'=>'idMesy.tajuk_mesy',
//                'headerOptions' => ['class' => 'nama_syarikat'],
//                'contentOptions' => ['class' => 'nama_syarikat'],
//            ],
//            [
//                'label'=>'No Daftar',
//                'attribute'=>'idAgseAgsv_no_daftar',
//                'value'=>'idAgseAgsv.no_daftar',
//                'headerOptions' => ['class' => 'nama_syarikat'],
//                'contentOptions' => ['class' => 'nama_syarikat'],
//            ],
//            'jenis_tajaan',
////            'kod_bidang',
////            'created_date',
////            'updated_date',
//            ['class' => 'yii\grid\ActionColumn'],
//        ],
//    ]);

//    GridView::widget([
//        'dataProvider' => $dataProvider,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//            'id',
//            [
//                'label'=>'Nama Syarikat',
//                'attribute'=>'idSyarikat.nama_syarikat',
//                'headerOptions' => ['class' => 'nama_syarikat'],
//                'contentOptions' => ['class' => 'nama_syarikat'],
//            ],
//            [
//                'label'=>'Tajuk Minit Mesy',
//                'attribute'=>'idMesy.idMinitMesies.tajuk_mesy',
//                'headerOptions' => ['class' => 'nama_syarikat'],
//                'contentOptions' => ['class' => 'nama_syarikat'],
//            ],
//            [
//                'label'=>'Nama Syarikat',
//                'attribute'=>'nama_syarikat',
//                'headerOptions' => ['class' => 'nama_syarikat'],
//                'contentOptions' => ['class' => 'nama_syarikat'],
//            ],
//            'jenis_tajaan',
//            'kod_bidang',
//            'created_date',
//            'updated_date',
//            ['class' => 'yii\grid\ActionColumn'],
//        ],
//    ]);
    ?>
    <input type='checkbox' id='nama_syarikat' checked>
</div>
<?php
$js = <<< JS
        $('#nama_syarikat').click(function() {
            $(".nama_syarikat").toggle(this.checked);
        });
        
JS;
$this->registerJs($js);




//
//$columns = 
//[
//    ['class'=>'kartik\grid\SerialColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
//    [
//        'attribute'=>'id',
////        'pageSummary'=>'Page Total',
//        'vAlign'=>'middle',
//        'order'=>DynaGrid::ORDER_FIX_LEFT
//    ],
//    [
//        'attribute'=>'idMesy.tajuk_mesy',
//        'vAlign'=>'middle',
//    ],
//    [
//        'attribute'=>'idAgseAgsv.no_daftar',
//        'vAlign'=>'middle',
//    ],
//    [
//        'attribute'=>'noKontrak.had_bumbung',
//        'vAlign'=>'middle',
//    ],
//    [
//        'attribute'=>'idSyarikat.nama_syarikat',
//        'vAlign'=>'middle',
//    ],
////    [
////        'attribute'=>'color',
////        'value'=>function ($model, $key, $index, $widget) {
////            return "<span class='badge' style='background-color: {$model->color}'> </span>  <code>" . 
////                $model->color . '</code>';
////        },
////        'filterType'=>GridView::FILTER_COLOR,
////        'filterWidgetOptions'=>[
////            'showDefaultPalette'=>false,
////            'pluginOptions'=>[
////                'showPalette'=>true,
////                'showPaletteOnly'=>true,
////                'showSelectionPalette'=>true,
////                'showAlpha'=>false,
////                'allowEmpty'=>false,
////                'preferredFormat'=>'name',
////                'palette'=>[
////                    [
////                        "white", "black", "grey", "silver", "gold", "brown", 
////                    ],
////                    [
////                        "red", "orange", "yellow", "indigo", "maroon", "pink"
////                    ],
////                    [
////                        "blue", "green", "violet", "cyan", "magenta", "purple", 
////                    ],
////                ]
////            ],
////        ],
////        'vAlign'=>'middle',
////        'format'=>'raw',
////        'width'=>'150px',
////        'noWrap'=>true
////    ],
//    [
//        'attribute'=>'created_date',
//        'filterType'=>GridView::FILTER_DATE,
//        'format'=>'raw',
//        'width'=>'170px',
//        'filterWidgetOptions'=>[
//            'pluginOptions'=>['format'=>'yyyy-mm-dd']
//        ],
//        'visible'=>false,
//    ],
////    [
////        'attribute'=>'id', 
////        'vAlign'=>'middle',
////        'width'=>'250px',
////        'value'=>function ($model, $key, $index, $widget) { 
////            return Html::a($model->author->name, '#', [
////                'title'=>'View author detail', 
////                'onclick'=>'alert("This will open the author page.\n\nDisabled for this demo!")'
////            ]);
////        },
////        'filterType'=>GridView::FILTER_SELECT2,
////        'filter'=>ArrayHelper::map(Author::find()->orderBy('name')->asArray()->all(), 'id', 'name'), 
////        'filterWidgetOptions'=>[
////            'pluginOptions'=>['allowClear'=>true],
////        ],
////        'filterInputOptions'=>['placeholder'=>'Any author'],
////        'format'=>'raw'
////    ],
////    [
////        'attribute'=>'buy_amount', 
////        'hAlign'=>'right', 
////        'vAlign'=>'middle',
////        'width'=>'100px',
////        'format'=>['decimal', 2],
////        'pageSummary'=>true
////    ],
////    [
////        'attribute'=>'sell_amount', 
////        'vAlign'=>'middle',
////        'hAlign'=>'right', 
////        'width'=>'100px',
////        'format'=>['decimal', 2],
////        'pageSummary'=>true
////    ],
////    [
////        'class'=>'kartik\grid\BooleanColumn',
////        'attribute'=>'status', 
////        'vAlign'=>'middle',
////    ],
////    [
////        'class'=>'kartik\grid\ActionColumn',
////        'dropdown'=>false,
////        'urlCreator'=>function($action, $model, $key, $index) { return '#'; },
////        'viewOptions'=>['title'=>$viewMsg, 'data-toggle'=>'tooltip'],
////        'updateOptions'=>['title'=>$updateMsg, 'data-toggle'=>'tooltip'],
////        'deleteOptions'=>['title'=>$deleteMsg, 'data-toggle'=>'tooltip'], 
////        'order'=>DynaGrid::ORDER_FIX_RIGHT
////    ],
//    ['class'=>'kartik\grid\CheckboxColumn', 'order'=>DynaGrid::ORDER_FIX_RIGHT],
//];
//$dynagrid = DynaGrid::begin([
//    'columns'=>$columns,
//    'theme'=>'panel-info',
//    'showPersonalize'=>true,
//    'storage'=>'cookie',
//    'gridOptions'=>[
//        'dataProvider'=>$dataProvider,
//        'filterModel'=>$searchModel,
//        'showPageSummary'=>true,
//        'floatHeader'=>true,
//        'pjax'=>true,
//        'panel'=>[
//            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  Library</h3>',
//            'before' =>  '<div style="padding-top: 7px;"><em>* The table header sticks to the top in this demo as you scroll</em></div>',
//            'after' => false
//        ],        
//        'toolbar' =>  [
//            ['content'=>
//                Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>'Add Book', 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['dynagrid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'Reset Grid'])
//            ],
//            ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
//            '{export}',
//        ]
//    ],
//    'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
//]);
//if (substr($dynagrid->theme, 0, 6) == 'simple') {
//    $dynagrid->gridOptions['panel'] = false;
//}  
//DynaGrid::end();
//
//
//
