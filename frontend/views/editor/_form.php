<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use kartik\widgets\ActiveForm;
//use kartik\builder\Form;
//use yii\bootstrap;
use dosamigos\datepicker\DatePicker;
//use frontend\models\MaklumatIndent;

//use kartik\date;
/* @var $this yii\web\View */
/* @var $model frontend\models\ButirKontrak */

/* @var $form yii\widgets\ActiveForm */
//$modelIndent = new MaklumatIndent();
?>

<div class="editor-create-form">
    
    <h1>Create Editor</h1>

    
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_indent')->textInput() ?>

    <?= $form->field($model, 'no_kontrak')->textInput(['maxlength' => true])->hint('Hint Text') ?>

    <?= $form->field($model, 'had_bumbung')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'tarikh_mula')->widget(
            DatePicker::className(), [
        'value' => date('Y-m-d H:i:s'),
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'tarikh_jv')->widget(
            DatePicker::className(), [
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'tarikh_sst')->widget(
            DatePicker::className(), [
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>



    <?=
    $form->field($model, 'tarikh_tt_kontrak')->widget(
            DatePicker::className(), [
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>
    <input type="checkbox" id="serah-terima" value="Show"> Edit Estimated Delivery Date
    &nbsp;&nbsp;
    <input type="checkbox" id="revenue_kontrak" value="Show"> Edit Revenue Kontrak 
    &nbsp;&nbsp;
    <input type="checkbox" id="jumlah_indent" value="Show"> Edit Jumlah Indent
    &nbsp;&nbsp;
    <input type="checkbox" id="baki_had_bumbung" value="Show"> Edit Baki Had Bumbung
    <br><br>

    <div class='revenue_kontrak' style='display:none;'>
        <?= $form->field($model, 'revenue_kontrak')->textInput() ?>
    </div>

    <div class='serah-terima' style='display:none;'>
        <?= $form->field($modelIndent, 'edd_serah')->textInput() ?>

        <?= $form->field($modelIndent, 'edd_terima')->textInput() ?>
    </div>

    <div class='jumlah_indent' style='display:none;'>
    <?= $form->field($modelIndent, 'jumlah_indent')->textInput() ?>
    </div>

    <div class='baki_had_bumbung' style='display:none;'>
    <?= $form->field($modelIndent, 'baki_had_bumbumg')->textInput() ?>
    </div>
    <!--////////////////////////////////////////////-->
        <!--carta org-->
    <?= $form->field($model, 'id_syarikat')->textInput() ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jawatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'level')->textInput(['maxlength' => true]) ?>

    
    
    <!--/////////////////////////////////////-->
    <!--maklumat agsv-->
    <?= $form->field($model, 'no_daftar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url_gambar')->fileInput() ?>

    <?= $form->field($model, 'tarikh_masuk_khidmat')->textInput() ?>

    <?= $form->field($model, 'tarikh_serah_terima')->textInput() ?>
    <!--/////////////////////////////////////-->
    <!--/////////////////////////////////////-->
    <!--/////////////////////////////////////-->
    <!--/////////////////////////////////////-->
    <!--/////////////////////////////////////-->
    <!--/////////////////////////////////////-->
    <!--/////////////////////////////////////-->
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$js = <<< JS
        $('#revenue_kontrak').click(function() {
            $(".revenue_kontrak").toggle(this.checked);
        });
        $('#serah-terima').click(function() {
            $(".serah-terima").toggle(this.checked);
        });
        $('#jumlah_indent').click(function() {
            $(".jumlah_indent").toggle(this.checked);
        });
        $('#baki_had_bumbung').click(function() {
            $(".baki_had_bumbung").toggle(this.checked);
        });
JS;
$this->registerJs($js);
