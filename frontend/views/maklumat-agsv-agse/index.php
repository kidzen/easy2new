<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MaklumatAgsvAgseSearch */ 
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Maklumat AGSV/AGSE';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maklumat-agsv-agse-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?> 
    
    <p>
        <?= Html::a('Create Maklumat AGSV/AGSE', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_daftar',
            'jenis_agsv_agse',
            [
                'label'=>'Gambar Kenderaan',
                'format'=>['image',['width'=>'auto','height'=>'100']],
                'value'=>'url_gambar',
//                'value'=>function($data) { return $data->url_gambar; },
            ],
            'tarikh_masuk_khidmat',
            'tarikh_serah_terima',
             'created_date',
             'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
