<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\MaklumatAgsvAgse */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Maklumat AGSV/AGSE', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maklumat-agsv-agse-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_daftar',
            'jenis_agsv_agse',
//            'url_gambar:url',
            [
                'label'=>'url_gambar',
                'format'=>['image',['width'=>'auto','height'=>'200']],
                'value'=>$model->url_gambar,
//                'value'=>function($model) { return $model->url_gambar; },
            ],
            'tarikh_masuk_khidmat',
            'tarikh_serah_terima',
            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>
