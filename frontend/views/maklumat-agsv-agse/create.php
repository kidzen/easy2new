<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\MaklumatAgsvAgse */

$this->title = 'Create Maklumat AGSV/AGSE';
$this->params['breadcrumbs'][] = ['label' => 'Maklumat AGSV/AGSE', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maklumat-agsv-agse-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
