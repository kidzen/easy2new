<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\CartaOrganisasiSyarikat */

$this->title = 'Create Carta Organisasi Syarikat';
$this->params['breadcrumbs'][] = ['label' => 'Carta Organisasi Syarikat', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carta-organisasi-syarikat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
