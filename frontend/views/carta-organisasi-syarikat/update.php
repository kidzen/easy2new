<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CartaOrganisasiSyarikat */

$this->title = 'Update Carta Organisasi Syarikat: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Carta Organisasi Syarikat', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="carta-organisasi-syarikat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
