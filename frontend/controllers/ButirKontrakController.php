<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ButirKontrak;
use frontend\models\ButirKontrakSearch;
use frontend\models\MaklumatIndent;
//use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Exception;

/**
 * ButirKontrakController implements the CRUD actions for ButirKontrak model.
 */
class ButirKontrakController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ButirKontrak models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new ButirKontrakSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ButirKontrak model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ButirKontrak model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
//        define model to fill
        $model = new ButirKontrak();
        $model->scenario = 'create';
        $modelIndent = new MaklumatIndent();

//      create kontrak prefix        
        $bilKontrak = ButirKontrak::find()
                ->where(['like', 'created_date', date('Y-m-d')])
                ->orderBy(['id' => SORT_DESC])->one();
        $bilKontrak = isset($bilKontrak)?substr($bilKontrak->no_indent, -3):0;
        $bilKontrak = $bilKontrak + 1;
        $bilKontrak = sprintf("%03d", $bilKontrak);

//        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $kontrak = MaklumatIndent::find()
                    ->where(['jenis_indent' => 'KONTRAK',])
                    ->andWhere(['no_kontrak' => $model->no_kontrak])
                    ->sum('jumlah_indent');
            $tajaan = MaklumatIndent::find()
                    ->where(['jenis_indent' => 'TAJAAN',])
                    ->andWhere(['no_kontrak' => $model->no_kontrak])
                    ->sum('jumlah_indent');
            $bakiKontrak = $kontrak - $tajaan + $model->revenue_kontrak;

            $model->had_bumbung = $bakiKontrak;

            if ($model->save()) {
                $modelIndent->no_indent = $model->no_indent;
                $modelIndent->no_kontrak = $model->no_kontrak;
                $modelIndent->jumlah_indent = $model->revenue_kontrak;
                $modelIndent->jenis_indent = 'KONTRAK';
//                $bakiLama = ButirKontrak::find()->orderBy('created_date DESC')->one();
//                $bakiLama = ButirKontrak::find()
//                        ->where(['no_kontrak' => $model->no_kontrak])
//                        ->sum('had_bumbung');
                $modelIndent->baki_had_bumbumg = $model->had_bumbung;
//                $model->had_bumbung = $bakiLama;
            }
            if ($model->save() && $modelIndent->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'bilKontrak' => $bilKontrak,
//                        'modelIndent' => $modelIndent,
            ]);
        }
    }

    /**
     * Updates an existing ButirKontrak model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

        $model = $this->findModel($id);
        $modelIndent = MaklumatIndent::find()
                ->where(['no_indent' => $model->no_indent])
                ->one();

        if ($model->load(Yii::$app->request->post())) {
//            $bakiKontrak = ButirKontrak::find()
//                            ->where(['no_kontrak' => $model->no_kontrak])
//                            ->orderBy('updated_date DESC')->one();
//
//            if ($bakiKontrak) {
//                $model->had_bumbung = $model->revenue_kontrak + $bakiKontrak->had_bumbung - $bakiKontrak->revenue_kontrak;
//            } else {
//                $model->had_bumbung = $model->revenue_kontrak;
//            }
            $kontrak = MaklumatIndent::find()
                    ->where(['jenis_indent' => 'KONTRAK',])
                    ->andWhere(['no_kontrak' => $model->no_kontrak])
                    ->sum('jumlah_indent');
            $tajaan = MaklumatIndent::find()
                    ->where(['jenis_indent' => 'TAJAAN',])
                    ->andWhere(['no_kontrak' => $model->no_kontrak])
                    ->sum('jumlah_indent');
            $bakiKontrak = $kontrak - $tajaan + $model->revenue_kontrak;

            $model->had_bumbung = $bakiKontrak;

            if ($model->save()) {

                $modelIndent->no_indent = $model->no_indent;
                $modelIndent->no_kontrak = $model->no_kontrak;
                $modelIndent->jumlah_indent = $model->revenue_kontrak;
                $modelIndent->jenis_indent = 'KONTRAK';
//                $bakiLama = ButirKontrak::find()
//                        ->where(['no_kontrak' => $model->no_kontrak])
//                        ->sum('had_bumbung');
                $modelIndent->baki_had_bumbumg = $model->had_bumbung;
//                $model->had_bumbung = $bakiLama;
            }
            if ($model->save() && $modelIndent->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
//            else {
//                throw new Exception('Fail to save');
//            }
        } else {
            return $this->render('update', [
                        'model' => $model,
//                        'modelIndent' => $modelIndent,
            ]);
        }
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                        'model' => $model,
//            ]);
//        }
    }

    /**
     * Deletes an existing ButirKontrak model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if ($this->relatedIndent($id)) {
            $this->relatedIndent($id)->delete();
            $this->findModel($id)->delete();
        } else {
            $this->findModel($id)->delete();
        }
//        $modelIndent = MaklumatIndent::find()
//                ->where(['no_indent'=>$model->no_indent])
//                ->one();
//        MaklumatIndent::findOne($modelIndent->id)->delete();
//        MaklumatIndent::deleteAll($modelIndent->id)

        return $this->redirect(['index']);
    }

    /**
     * Finds the ButirKontrak model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ButirKontrak the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ButirKontrak::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function relatedIndent($id) {
        if (($model = ButirKontrak::findOne($id)) !== null) {
            $modelIndent = MaklumatIndent::find()
                    ->where(['no_indent' => $model->no_indent])
                    ->one();
            return $modelIndent;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
