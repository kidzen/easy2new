<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\MaklumatAgsvAgse;

/**
 * MaklumatAgsvAgseSearch represents the model behind the search form about `frontend\models\MaklumatAgsvAgse`.
 */
class MaklumatAgsvAgseSearch extends MaklumatAgsvAgse
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['no_daftar', 'url_gambar', 'jenis_agsv_agse', 'tarikh_masuk_khidmat', 'tarikh_serah_terima', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MaklumatAgsvAgse::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tarikh_masuk_khidmat' => $this->tarikh_masuk_khidmat,
            'tarikh_serah_terima' => $this->tarikh_serah_terima,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'no_daftar', $this->no_daftar])
            ->andFilterWhere(['like', 'url_gambar', $this->url_gambar])
            ->andFilterWhere(['like', 'jenis_agsv_agse', $this->jenis_agsv_agse]);

        return $dataProvider;
    }
}
