<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\TajaanMesy;

/**
 * TajaanMesySearch represents the model behind the search form about `frontend\models\TajaanMesy`.
 */
class DataSearch extends TajaanMesy {

    public $idMesy_tajuk;
    public $idAgseAgsv_no_daftar;
    public $idMesy_siri;
    //Butir Kontrak
    public $kontrak_no_indent;
    public $kontrak_no_kontrak;
    public $kontrak_had_bumbung;
    public $kontrak_tarikh_mula;
    public $kontrak_tarikh_tamat_kontrak;
    public $kontrak_revenue_kontrak;
    //Mesy Jawatan Kuasa
    public $mesy_id;
    public $mesy_tajuk_mesy;
    public $mesy_tarikh_mesy;
    public $mesy_id_lst_ahli;
    public $mesy_siri;
    //Syarikat
    public $syarikat_id;
    public $syarikat_nama_syarikat;
    public $syarikat_kod_bidang;
    //kenderaan
    public $kenderaan_id;
    public $kenderaan_no_daftar;
    public $kenderaan_url_gambar;
    public $kenderaan_tarikh_masuk_khidmat;
    public $kenderaan_tarikh_serah_terima;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            //tajaan
            [['id', 'id_syarikat'], 'integer'],
            [['id_mesy', 'no_kontrak', 'jenis_tajaan', 'created_date', 'updated_date', 'id_agse_agsv'], 'safe'],
            [['harga'], 'number'],
            //kontrak
            [['kontrak_no_indent', 'kontrak_no_kontrak', 'kontrak_had_bumbung',
                'kontrak_tarikh_mula', 'kontrak_tarikh_tamat_kontrak', 
                'kontrak_revenue_kontrak',], 'safe'],
            //Syarikat
            [['syarikat_id', 'syarikat_nama_syarikat', 'syarikat_kod_bidang'], 'safe'],
            //Mesy
            [['mesy_siri', 'mesy_id_lst_ahli', 'mesy_tarikh_mesy','mesy_tajuk_mesy'], 'safe'],
            //Kenderaan
            [['kenderaan_no_daftar', 'kenderaan_url_gambar', 'kenderaan_tarikh_masuk_khidmat',
                'kenderaan_tarikh_serah_terima'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
//        $query = TajaanMesy::find();
//        $query->joinWith('idAgseAgsv');
//        $query->joinWith('idMesy');
//        $query->joinWith('idSyarikat');
//        $query->joinWith('noIndent');

        $query = MaklumatIndent::find();
        $query->joinWith('idAgseAgsv');
        $query->joinWith('idMesy');
        $query->joinWith('idSyarikat');
        $query->joinWith('noIndent');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        //Sejarah
        $dataProvider->sort->attributes['sej_jenis_baiki'] = [
            'asc' => ['maklumat_agsv_agse.jenis_pembaikian' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.jenis_pembaikian' => SORT_DESC],
        ];
        //indent
        $dataProvider->sort->attributes['indent_no_indent'] = [
            'asc' => ['maklumat_indent.no_indent' => SORT_ASC],
            'desc' => ['maklumat_indent.no_indent' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['indent_jenis_indent'] = [
            'asc' => ['maklumat_indent.jenis_indent' => SORT_ASC],
            'desc' => ['maklumat_indent.jenis_indent' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['indent_jumlah_indent'] = [
            'asc' => ['maklumat_indent.jumlah_indent' => SORT_ASC],
            'desc' => ['maklumat_indent.jumlah_indent' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['indent_baki_had_bumbung'] = [
            'asc' => ['maklumat_indent.baki_had_bumbung' => SORT_ASC],
            'desc' => ['maklumat_indent.baki_had_bumbung' => SORT_DESC],
        ];
        //syarikat
        $dataProvider->sort->attributes['syarikat_nama_syarikat'] = [
            'asc' => ['maklumat_syarikat.nama_syarikat' => SORT_ASC],
            'desc' => ['maklumat_syarikat.nama_syarikat' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['syarikat_kod_bidang'] = [
            'asc' => ['maklumat_syarikat.kod_bidang' => SORT_ASC],
            'desc' => ['maklumat_syarikat.kod_bidang' => SORT_DESC],
        ];
        //kontrak
        $dataProvider->sort->attributes['kontrak_no_kontrak'] = [
            'asc' => ['butir_kontrak.no_kontrak' => SORT_ASC],
            'desc' => ['butir_kontrak.no_kontrak' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kontrak_had_bumbung'] = [
            'asc' => ['butir_kontrak.had_bumbung' => SORT_ASC],
            'desc' => ['butir_kontrak.had_bumbung' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kontrak_tarikh_mula'] = [
            'asc' => ['butir_kontrak.tarikh_mula' => SORT_ASC],
            'desc' => ['butir_kontrak.tarikh_mula' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kontrak_tarikh_tamat_kontrak'] = [
            'asc' => ['butir_kontrak.tarikh_tamat_kontrak' => SORT_ASC],
            'desc' => ['butir_kontrak.tarikh_tamat_kontrak' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kontrak_revenue_kontrak'] = [
            'asc' => ['butir_kontrak.revenue_kontrak' => SORT_ASC],
            'desc' => ['butir_kontrak.revenue_kontrak' => SORT_DESC],
        ];
        //mesy
        $dataProvider->sort->attributes['mesy_tajuk_mesy'] = [
            'asc' => ['mesy_jawatan_kuasa.tajuk_mesy' => SORT_ASC],
            'desc' => ['mesy_jawatan_kuasa.tajuk_mesy' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['mesy_tarikh_mesy'] = [
            'asc' => ['mesy_jawatan_kuasa.tarikh_mesy' => SORT_ASC],
            'desc' => ['mesy_jawatan_kuasa.tarikh_mesy' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['mesy_id_lst_ahli'] = [
            'asc' => ['mesy_jawatan_kuasa.id_lst_ahli' => SORT_ASC],
            'desc' => ['mesy_jawatan_kuasa.id_lst_ahli' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['mesy_siri'] = [
            'asc' => ['mesy_jawatan_kuasa.siri' => SORT_ASC],
            'desc' => ['mesy_jawatan_kuasa.siri' => SORT_DESC],
        ];
        //kenderaan
        $dataProvider->sort->attributes['kenderaan_no_daftar'] = [
            'asc' => ['maklumat_agsv_agse.no_daftar' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.no_daftar' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kenderaan_url_gambar'] = [
            'asc' => ['maklumat_agsv_agse.url_gambar' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.url_gambar' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kenderaan_tarikh_masuk_khidmat'] = [
            'asc' => ['maklumat_agsv_agse.tarikh_masuk_khidmat' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.tarikh_masuk_khidmat' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kenderaan_tarikh_serah_terima'] = [
            'asc' => ['maklumat_agsv_agse.tarikh_serah_terima' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.tarikh_serah_terima' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            //tajaan 
            'tajaan_mesy.id' => $this->id,
            'tajaan_mesy.id_mesy' => $this->id_mesy,
//            'tajaan_mesy.mesy_jawatan_kuasa.siri' => $this->id_mesy,
            'tajaan_mesy.id_syarikat' => $this->id_syarikat,
            'tajaan_mesy.id_agse_agsv' => $this->id_agse_agsv,
            'tajaan_mesy.harga' => $this->harga,
            'tajaan_mesy.created_date' => $this->created_date,
            'tajaan_mesy.updated_date' => $this->updated_date,
            'maklumat_syarikat.kod_bidang' => $this->syarikat_kod_bidang,
            'mesy_jawatan_kuasa.id_lst_ahli' => $this->mesy_id_lst_ahli
                //
        ]);

        $query->andFilterWhere(['like', 'jenis_tajaan', $this->jenis_tajaan])

                //kontrak
                ->andFilterWhere(['like', 'butir_kontrak.no_kontrak', $this->kontrak_no_kontrak])
                ->andFilterWhere(['like', 'butir_kontrak.revenue_kontrak', $this->kontrak_revenue_kontrak])
                ->andFilterWhere(['like', 'butir_kontrak.no_kontrak', $this->kontrak_no_kontrak])
                ->andFilterWhere(['like', 'butir_kontrak.revenue_kontrak', $this->kontrak_revenue_kontrak])
                //syarikat
//                ->andFilterWhere(['like', 'maklumat_syarikat.kod_bidang', $this->syarikat_kod_bidang])
                ->andFilterWhere(['like', 'maklumat_syarikat.nama_syarikat', $this->syarikat_nama_syarikat])
                //mesy
                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.tajuk_mesy', $this->mesy_tajuk_mesy])
                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.tarikh_mesy', $this->mesy_tarikh_mesy])
                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.id_lst_ahli', $this->mesy_id_lst_ahli])
                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.siri', $this->mesy_siri])
                //kenderaan
                ->andFilterWhere(['like', 'maklumat_agsv_agse.no_daftar', $this->kenderaan_no_daftar])
                ->andFilterWhere(['like', 'maklumat_agsv_agse.url_gambar', $this->kenderaan_url_gambar])
                ->andFilterWhere(['like', 'maklumat_agsv_agse.tarikh_masuk_khidmat', $this->kenderaan_tarikh_masuk_khidmat])
                ->andFilterWhere(['like', 'maklumat_agsv_agse.tarikh_serah_terima', $this->kenderaan_tarikh_serah_terima])
                ;
//            ->andFilterWhere(['like', 'mesy_jawatan_kuasa.tajuk_mesy', $this->idMesy_tajuk])
//            ->andFilterWhere(['like', 'mesy_jawatan_kuasa.siri', $this->idMesy_siri])
//            ->andFilterWhere(['like', 'maklumat_agsv_agse.no_daftar', $this->idAgseAgsv_no_daftar]);
//            ->andFilterWhere(['like', 'maklumat_agsv_agse.no_daftar', $this->idAgseAgsv]);

        return $dataProvider;
    }

}
