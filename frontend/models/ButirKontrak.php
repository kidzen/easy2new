<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use frontend\models\MaklumatIndent;
use frontend\models\MaklumatSyarikat;
use frontend\models\TajaanMesy;
//use yii\validators\UniqueValidator;


/**
 * This is the model class for table "butir_kontrak".
 *
 * @property integer $id
 * @property string $no_indent
 * @property string $no_kontrak
 * @property string $had_bumbung
 * @property double $revenue_kontrak
 * @property string $tarikh_mula
 * @property string $tarikh_tamat_kontrak
 * @property string $created_date
 * @property string $updated_date
 *
 * @property TajaanMesy[] $tajaanMesies
 */
class ButirKontrak extends \yii\db\ActiveRecord {

    public function behaviors() {
        return [

//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_date',
//                'updatedAtAttribute' => 'updated_date',
//                'value' => date('Y-m-d H:i:s'),
//            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_date', 'updated_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_date'],
                ],
//                     'value' => date('Y-m-d H:i:s'),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'butir_kontrak';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['revenue_kontrak','had_bumbung','id_syarikat'], 'number'],
            [['tarikh_mula', 'tarikh_tamat_kontrak', 'created_date', 'updated_date'], 'safe'],
//            [['no_indent'], 'unique'],
            [['no_indent','no_kontrak'], 'required'],
            [['no_indent', 'no_kontrak'], 'string', 'max' => 50],
            [['no_indent'], 'unique',
                'on' => 'create', 
                'targetClass' => 'frontend\models\MaklumatIndent', 
////                'targetAttribute' => ['no_indent' => 'no_indent'], 
//                'message' => 'This indent no has already exist.'
                ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'no_indent' => 'No Indent',
            'no_kontrak' => 'No Kontrak',
            'id_syarikat' => 'Id Syarikat',
            'had_bumbung' => 'Had Bumbung',
            'revenue_kontrak' => 'Revenue Kontrak',
            'tarikh_mula' => 'Tarikh Mula',
            'tarikh_tamat_kontrak' => 'Tarikh Tamat Kontrak',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTajaanMesies() {
        return $this->hasMany(TajaanMesy::className(), ['no_kontrak' => 'no_kontrak']);
    }
    
    public function getIdIndent() {
        return $this->hasMany(MaklumatIndent::className(), ['no_indent' => 'no_indent']);
    }
    
    public function getIdSyarikat() {
        return $this->hasOne(MaklumatSyarikat::className(), ['id' => 'id_syarikat']);
    }

}
