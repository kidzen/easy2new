<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\CartaOrganisasiSyarikat;

/**
 * CartaOrganisasiSyarikatSearch represents the model behind the search form about `frontend\models\CartaOrganisasiSyarikat`.
 */
class CartaOrganisasiSyarikatSearch extends CartaOrganisasiSyarikat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_syarikat'], 'integer'],
            [['nama', 'jawatan', 'level', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CartaOrganisasiSyarikat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_syarikat' => $this->id_syarikat,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jawatan', $this->jawatan])
            ->andFilterWhere(['like', 'level', $this->level]);

        return $dataProvider;
    }
}
