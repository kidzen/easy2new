<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;


/**
 * This is the model class for table "mesy_jawatan_kuasa".
 *
 * @property integer $id
 * @property string $tajuk_mesy
 * @property string $tarikh_mesy
 * @property string $id_lst_ahli
 * @property string $siri
 * @property string $created_date
 * @property string $updated_date
 *
 * @property MinitMesy[] $minitMesies
 * @property TajaanMesy[] $tajaanMesies
 */
class MesyJawatanKuasa extends \yii\db\ActiveRecord
{
    public function behaviors() {
        return [
            
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_date',
//                'updatedAtAttribute' => 'updated_date',
//                'value' => date('Y-m-d H:i:s'),
//            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_date', 'updated_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_date'],
                ],
//                     'value' => date('Y-m-d H:i:s'),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mesy_jawatan_kuasa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tarikh_mesy', 'created_date', 'updated_date'], 'safe'],
            [['tajuk_mesy', 'ahli1', 'ahli2', 'ahli3', 'siri'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tajuk_mesy' => 'Tajuk Mesy',
            'tarikh_mesy' => 'Tarikh Mesy',
            'ahli1' => 'Ahli 1',
            'ahli2' => 'Ahli 2',
            'ahli3' => 'Ahli 3',
            'siri' => 'Siri',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMinitMesies()
    {
        return $this->hasMany(MinitMesy::className(), ['id_mesy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTajaanMesies()
    {
        return $this->hasMany(TajaanMesy::className(), ['id_mesy' => 'id']);
    }
}
