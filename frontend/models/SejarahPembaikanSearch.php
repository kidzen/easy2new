<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\SejarahPembaikan;

/**
 * SejarahPembaikanSearch represents the model behind the search form about `frontend\models\SejarahPembaikan`.
 */
class SejarahPembaikanSearch extends SejarahPembaikan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_agsv_agse', 'qty_alat_ganti'], 'integer'],
            [['no_indent', 'jenis_pembaikan', 'lst_alat_ganti', 'tarikh_terima', 'tarikh_siap', 'created_date', 'updated_date'], 'safe'],
            [['harga_alat_ganti'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SejarahPembaikan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_agsv_agse' => $this->id_agsv_agse,
            'qty_alat_ganti' => $this->qty_alat_ganti,
            'harga_alat_ganti' => $this->harga_alat_ganti,
            'tarikh_terima' => $this->tarikh_terima,
            'tarikh_siap' => $this->tarikh_siap,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'no_indent', $this->no_indent])
            ->andFilterWhere(['like', 'jenis_pembaikan', $this->jenis_pembaikan])
            ->andFilterWhere(['like', 'lst_alat_ganti', $this->lst_alat_ganti]);

        return $dataProvider;
    }
}
