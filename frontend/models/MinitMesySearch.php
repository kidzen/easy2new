<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\MinitMesy;

/**
 * MinitMesySearch represents the model behind the search form about `frontend\models\MinitMesy`.
 */
class MinitMesySearch extends MinitMesy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_mesy'], 'integer'],
            [['tajuk_mesy', 'mula', 'habis', 'created_date', 'updated_date'], 'safe'],
            [['jangkamasa'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MinitMesy::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_mesy' => $this->id_mesy,
            'mula' => $this->mula,
            'habis' => $this->habis,
            'jangkamasa' => $this->jangkamasa,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'tajuk_mesy', $this->tajuk_mesy]);

        return $dataProvider;
    }
}
