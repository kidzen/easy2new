<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\MaklumatIndent;

/**
 * MaklumatIndentSearch represents the model behind the search form about `frontend\models\MaklumatIndent`.
 */
class MaklumatIndentSearch extends MaklumatIndent {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id'], 'integer'],
            [['no_indent','no_kontrak', 'jenis_indent', 'created_date', 'updated_date'], 'safe'],
            [['jumlah_indent', 'baki_had_bumbumg'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MaklumatIndent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'jumlah_indent' => $this->jumlah_indent,
            'baki_had_bumbumg' => $this->baki_had_bumbumg,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'no_indent', $this->no_indent])
                ->andFilterWhere(['like', 'no_kontrak', $this->no_kontrak])
                ->andFilterWhere(['like', 'jenis_indent', $this->jenis_indent]);

        return $dataProvider;
    }

}
