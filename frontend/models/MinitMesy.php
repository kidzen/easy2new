<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "minit_mesy".
 *
 * @property integer $id
 * @property integer $id_mesy
 * @property string $tajuk_mesy
 * @property string $mula
 * @property string $habis
 * @property double $jangkamasa
 * @property string $created_date
 * @property string $updated_date
 *
 * @property MesyJawatanKuasa $idMesy
 */
class MinitMesy extends \yii\db\ActiveRecord
{
    public function behaviors() {
        return [
            
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_date',
//                'updatedAtAttribute' => 'updated_date',
//                'value' => date('Y-m-d H:i:s'),
//            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_date', 'updated_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_date'],
                ],
//                     'value' => date('Y-m-d H:i:s'),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'minit_mesy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_mesy'], 'integer'],
            [['mula', 'habis', 'created_date', 'updated_date'], 'safe'],
            [['jangkamasa'], 'number'],
            [['tajuk_mesy'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_mesy' => 'Id Mesy',
            'tajuk_mesy' => 'Tajuk Mesy',
            'mula' => 'Mula',
            'habis' => 'Habis',
            'jangkamasa' => 'Jangkamasa',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMesy()
    {
        return $this->hasOne(MesyJawatanKuasa::className(), ['id' => 'id_mesy']);
    }
}
