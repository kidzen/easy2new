<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\ButirKontrak;

/**
 * ButirKontrakSearch represents the model behind the search form about `frontend\models\ButirKontrak`.
 */
class ButirKontrakSearch extends ButirKontrak {

    /**
     * @inheritdoc
     */
    public $syarikat_nama_syarikat;
    
    public function rules() {
        return [
            [['id','id_syarikat'], 'integer'],
            [['no_indent', 'no_kontrak', 'id_syarikat', 'had_bumbung','syarikat_nama_syarikat', 'tarikh_mula',
            'tarikh_tamat_kontrak', 'created_date', 'updated_date'], 'safe'],
            [['revenue_kontrak'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ButirKontrak::find();
        $query->joinWith('idSyarikat');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort->attributes['syarikat_nama_syarikat'] = [
            'asc' => ['maklumat_syarikat.nama_syarikat' => SORT_ASC],
            'desc' => ['maklumat_syarikat.nama_syarikat' => SORT_DESC],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tarikh_mula' => $this->tarikh_mula,
            'tarikh_tamat_kontrak' => $this->tarikh_tamat_kontrak,
            'revenue_kontrak' => $this->revenue_kontrak,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'no_indent', $this->no_indent])
                ->andFilterWhere(['like', 'no_kontrak', $this->no_kontrak])
                ->andFilterWhere(['like', 'maklumat_syarikat.nama_syarikat', $this->syarikat_nama_syarikat])
                ->andFilterWhere(['like', 'had_bumbung', $this->had_bumbung]);

        return $dataProvider;
    }

}
