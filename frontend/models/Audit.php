<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "audit".
 *
 * @property integer $id
 * @property string $table_name
 * @property string $column_name
 * @property string $old_data
 * @property string $new_data
 * @property string $time_stamp
 */
class Audit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_date',
//                'updatedAtAttribute' => 'updated_date',
//                'value' => date('Y-m-d H:i:s'),
//            ],
//            'timestamp' => [
//                'class' => 'yii\behaviors\TimestampBehavior',
//                'attributes' => [
//                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
//                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
//                ],
//            ],
        ];
    }
    
    public static function tableName()
    {
        return 'audit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time_stamp'], 'safe'],
            [['table_name', 'column_name', 'old_data', 'new_data'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => 'Table Name',
            'column_name' => 'Column Name',
            'old_data' => 'Old Data',
            'new_data' => 'New Data',
            'time_stamp' => 'Time Stamp',
        ];
    }
}
