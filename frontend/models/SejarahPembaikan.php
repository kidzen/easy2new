<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;


/**
 * This is the model class for table "sejarah_pembaikan".
 *
 * @property integer $id
 * @property string $no_indent
 * @property integer $id_agsv_agse
 * @property string $jenis_pembaikan
 * @property string $lst_alat_ganti
 * @property integer $qty_alat_ganti
 * @property double $harga_alat_ganti
 * @property string $tarikh_terima
 * @property string $tarikh_siap
 * @property string $created_date
 * @property string $updated_date
 *
 * @property MaklumatIndent $idIndent
 * @property MaklumatAgsvAgse $idAgsvAgse
 */
class SejarahPembaikan extends \yii\db\ActiveRecord
{
    public function behaviors() {
        return [
            
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_date',
//                'updatedAtAttribute' => 'updated_date',
//                'value' => date('Y-m-d H:i:s'),
//            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_date', 'updated_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_date'],
                ],
//                     'value' => date('Y-m-d H:i:s'),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sejarah_pembaikan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_agsv_agse', 'qty_alat_ganti'], 'integer'],
            [['harga_alat_ganti'], 'number'],
            [['tarikh_terima', 'tarikh_siap', 'created_date', 'updated_date'], 'safe'],
            [['no_indent', 'jenis_pembaikan', 'lst_alat_ganti'], 'string', 'max' => 50],
            ['no_indent', 'unique', 'on' => 'insert','targetClass' => '\frontend\models\MaklumatIndent', 
                'message' => 'This indent no has already exist.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_indent' => 'No Indent',
            'id_agsv_agse' => 'Id Agsv Agse',
            'jenis_pembaikan' => 'Jenis Pembaikan',
            'lst_alat_ganti' => 'Lst Alat Ganti',
            'qty_alat_ganti' => 'Qty Alat Ganti',
            'harga_alat_ganti' => 'Harga Alat Ganti',
            'tarikh_terima' => 'Tarikh Terima',
            'tarikh_siap' => 'Tarikh Siap',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoIndent()
    {
        return $this->hasOne(MaklumatIndent::className(), ['id' => 'no_indent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAgsvAgse()
    {
        return $this->hasOne(MaklumatAgsvAgse::className(), ['id' => 'id_agsv_agse']);
    }
}
