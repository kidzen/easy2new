<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "maklumat_agsv_agse".
 *
 * @property integer $id
 * @property string $no_daftar
 * @property string $url_gambar
 * @property string $tarikh_masuk_khidmat
 * @property string $tarikh_serah_terima
 * @property string $created_date
 * @property string $updated_date
 *
 * @property SejarahPembaikan[] $sejarahPembaikans
 * @property TajaanMesy[] $tajaanMesies
 */
class MaklumatAgsvAgse extends \yii\db\ActiveRecord
{
//    public $file;
    public function behaviors() {
        return [
            
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_date',
//                'updatedAtAttribute' => 'updated_date',
//                'value' => date('Y-m-d H:i:s'),
//            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_date', 'updated_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_date'],
                ],
//                     'value' => date('Y-m-d H:i:s'),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'maklumat_agsv_agse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_agsv_agse','tarikh_masuk_khidmat', 'tarikh_serah_terima', 'created_date', 'updated_date'], 'safe'],
            [['no_daftar'], 'string', 'max' => 50],
            [['url_gambar','jenis_agsv_agse'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_daftar' => 'No Daftar',
            'jenis_agsv_agse' => 'Jenis AGSV/AGSE',
            'url_gambar' => 'Url Gambar',
            'tarikh_masuk_khidmat' => 'Tarikh Masuk Khidmat',
            'tarikh_serah_terima' => 'Tarikh Serah Terima',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSejarahPembaikans()
    {
        return $this->hasMany(SejarahPembaikan::className(), ['id_agsv_agse' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTajaanMesies()
    {
        return $this->hasMany(TajaanMesy::className(), ['id_agse_agsv' => 'id']);
    }
}
