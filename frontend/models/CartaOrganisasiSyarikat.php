<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
/**
 * This is the model class for table "carta_organisasi_syarikat".
 *
 * @property integer $id
 * @property integer $id_syarikat
 * @property string $nama
 * @property string $jawatan
 * @property string $level
 * @property string $created_date
 * @property string $updated_date
 *
 * @property MaklumatSyarikat $idSyarikat
 */
class CartaOrganisasiSyarikat extends \yii\db\ActiveRecord
{
    public function behaviors() {
        return [
            
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_date',
//                'updatedAtAttribute' => 'updated_date',
//                'value' => date('Y-m-d H:i:s'),
//            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_date', 'updated_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_date'],
                ],
//                     'value' => date('Y-m-d H:i:s'),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carta_organisasi_syarikat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_syarikat'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['nama', 'jawatan', 'level'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_syarikat' => 'Id Syarikat',
            'nama' => 'Nama',
            'jawatan' => 'Jawatan',
            'level' => 'Level',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSyarikat()
    {
        return $this->hasOne(MaklumatSyarikat::className(), ['id' => 'id_syarikat']);
    }
}
