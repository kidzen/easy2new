<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\TajaanMesy;

/**
 * TajaanMesySearch represents the model behind the search form about `frontend\models\TajaanMesy`.
 */
class TajaanMesySearch extends TajaanMesy {

    public $mesy_tajuk_mesy;
    public $syarikat_nama_syarikat;
    public $kenderaan_no_daftar;
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'id_mesy', 'id_agse_agsv'], 'integer'],
            [['no_indent', 'mesy_tajuk_mesy','syarikat_nama_syarikat',
                'kenderaan_no_daftar','no_kontrak', 'jenis_tajaan', 
                'edd_serah', 'edd_terima', 'created_date', 'updated_date'], 'safe'],
            [['harga'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = TajaanMesy::find();
        $query->joinWith('idMesy');
        $query->joinWith('idAgseAgsv');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['mesy_tajuk_mesy'] = [
            'asc' => ['mesy_jawatan_kuasa.tajuk_mesy' => SORT_ASC],
            'desc' => ['mesy_jawatan_kuasa.tajuk_mesy' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kenderaan_no_daftar'] = [
            'asc' => ['maklumat_agsv_agse.no_daftar' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.no_daftar' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['syarikat_nama_syarikat'] = [
            'asc' => ['maklumat_syarikat.nama_syarikat' => SORT_ASC],
            'desc' => ['maklumat_syarikat.nama_syarikat' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
//            'id_mesy' => $this->id_mesy,
//            'id_syarikat' => $this->id_syarikat,
//            'id_agse_agsv' => $this->id_agse_agsv,
            'harga' => $this->harga,
            'edd_serah' => $this->edd_serah,
            'edd_terima' => $this->edd_terima,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'no_indent', $this->no_indent])
                ->andFilterWhere(['like', 'no_kontrak', $this->no_kontrak])
                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.tajuk_mesy', $this->mesy_tajuk_mesy])
                ->andFilterWhere(['like', 'maklumat_agsv_agse.no_daftar', $this->kenderaan_no_daftar])
                ->andFilterWhere(['like', 'jenis_tajaan', $this->jenis_tajaan]);

        return $dataProvider;
    }

}
