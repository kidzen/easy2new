<?php

use yii\db\Migration;

class m161006_204900_maklumat_indent extends Migration
{
    
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%maklumat_indent}}', [
            'id' => $this->primaryKey(),
            'no_indent' => $this->string()->unique(),
            'no_kontrak' => $this->string(),
            'jenis_indent' => $this->string(),
            'jumlah_indent' => $this->double(),
            'baki_had_bumbumg' => $this->double(),
            'created_date' => $this->dateTime(),
            'updated_date' => $this->dateTime(),
                ], $tableOptions);
    }

    public function down() {
        $this->dropTable('{{%maklumat_indent}}');
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
