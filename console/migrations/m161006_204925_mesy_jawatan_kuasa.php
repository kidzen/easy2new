<?php

use yii\db\Migration;

class m161006_204925_mesy_jawatan_kuasa extends Migration
{
   
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%mesy_jawatan_kuasa}}', [
            'id' => $this->primaryKey(),
            'tajuk_mesy' => $this->string(),
            'tarikh_mesy' => $this->dateTime(),
            'ahli1' => $this->string(),
            'ahli2' => $this->string(),
            'ahli3' => $this->string(),
            'siri' => $this->string(),
            'created_date' => $this->dateTime(),
            'updated_date' => $this->dateTime(),
                ], $tableOptions);
    }

    public function down() {
        $this->dropTable('{{%mesy_jawatan_kuasa}}');
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
