<?php

use yii\db\Migration;

class m161006_213044_data_migrate extends Migration {

  public function up() {
    //Add data from Sql Dump File
   // $this->getDataFromSqlDump();

   // Add super administrator
    echo "Registering SuperAdmin.... \n";
    $this->RegisterAdmin();
    echo "Done registering SuperAdmin. \n\n";

    echo "Registering user1 and user2.... \n";
    $this->RegisterOthers();
    echo "Done registering user1 and user2. \n\n";

  }

  private function getDataFromSqlDump() {
    $fileName = 'sistemgk4.sql';
    $myfile = fopen($fileName, "r") or die("Unable to open file!");
    $commandR = fread($myfile, filesize($fileName));
    fclose($myfile);

    $sql = $commandR;

    foreach (array_filter(array_map('trim', explode(';', $sql))) as $query) {
      $this->execute($query);
    }
  }

  private function RegisterAdmin() {

//        $date = new DateTime();
//        $date->setTimestamp(time());
//        $time = $date->format('Y-m-d H:i:s');

    $password_hash = \Yii::$app->getSecurity()->generatePasswordHash("admin2");
    $auth_key = \Yii::$app->security->generateRandomString();

    $this->insert('{{%user}}', [
      'username' => 'admin2',
      'auth_key' => $auth_key,
      'password_hash' => $password_hash,
      'email' => 'admin2@gmail.com',
      'role' => 'Administrator',
      'status' => 10,
      'created_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
      'updated_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
      ]);
  }

  private function RegisterOthers() {

    $password_hash = Yii::$app->getSecurity()->generatePasswordHash("admin2");
    $auth_key = Yii::$app->security->generateRandomString();
    $this->insert('{{%user}}', [
      'username' => 'user1',
      'auth_key' => $auth_key,
      'password_hash' => $password_hash,
      'email' => 'user1@mail.com',
      'role' => 'User',
      'status' => 10,
      'created_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
      'updated_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
      ]);

    $password_hash = Yii::$app->getSecurity()->generatePasswordHash("admin2");
    $auth_key = Yii::$app->security->generateRandomString();
    $this->insert('{{%user}}', [
      'username' => 'user2',
      'auth_key' => $auth_key,
      'password_hash' => $password_hash,
      'email' => 'user2@mail.com',
      'role' => 'User',
      'status' => 10,
      'created_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
      'updated_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
      ]);
  }


  public function down() {
   $this->truncateTable('user');
 }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
    }
