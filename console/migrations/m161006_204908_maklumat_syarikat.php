<?php

use yii\db\Migration;

class m161006_204908_maklumat_syarikat extends Migration
{
   
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%maklumat_syarikat}}', [
            'id' => $this->primaryKey(),
            'nama_syarikat' => $this->string(),
            'kod_bidang' => $this->string(),
            'created_date' => $this->dateTime(),
            'updated_date' => $this->dateTime(),
                ], $tableOptions);
    }

    public function down() {
        $this->dropTable('{{%maklumat_syarikat}}');
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
