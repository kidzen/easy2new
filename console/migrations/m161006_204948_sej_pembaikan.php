<?php

use yii\db\Migration;

class m161006_204948_sej_pembaikan extends Migration
{
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sejarah_pembaikan}}', [
            'id' => $this->primaryKey(),
            'no_indent' => $this->string(),
            'id_agsv_agse' => $this->integer(),
            'jenis_pembaikan' => $this->string(),
            'lst_alat_ganti' => $this->string(),
            'qty_alat_ganti' => $this->integer(),
            'harga_alat_ganti' => $this->double(),
            'tarikh_terima' => $this->date(),
            'tarikh_siap' => $this->date(),
            'created_date' => $this->dateTime(),
            'updated_date' => $this->dateTime(),
                ], $tableOptions);
    }

    public function down() {
        $this->dropTable('{{%sejarah_pembaikan}}');
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
